import React from 'react';
import { render } from 'react-dom';
import Theme, { theme } from 'utils/Theme';
import Provider, { store } from 'utils/Provider';
import { loadProfile } from 'utils/actions';
import App from 'components/songs/App';

if (process.browser && process.env.NODE_ENV !== 'production') {
  // debug
  window.theme = theme;
  window.store = store;
}

// fetch currentUser data or show signin, early
store.dispatch(loadProfile());

render(
  <Provider>
    <Theme>
      <App />
    </Theme>
  </Provider>,
  document.getElementById('root'),
);
