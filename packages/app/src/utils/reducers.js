import { Map } from 'immutable';
import {
  ALL, // dispatch multiple actions at once
  FLASH, // flash messages, displayed in snackbars notifications
  CURRENT_USER,
  DATA,
  CLEAR_DATA,
  CLEAR_ALL_DATA,
} from './actions';

const initialState = {
  data: {
    songs: Map(),
  },
};

const reducers = {
  [FLASH]: (state, data) => ({ ...state, flash: data }),
  [CURRENT_USER]: (state, currentUser) => ({
    ...state,
    currentUser,
  }),
  [DATA]: (state, data) => {
    // merge data in state.data
    // for now only handle songs or song keys, both in 'songs' so only one key in data for now, other keys could be handled similarly here
    const newData = { ...state.data };

    if (Array.isArray(data.songs)) {
      data.songs.forEach(song => {
        newData.songs = newData.songs.set(song.id, song);
      });
    } else if (data.song !== undefined) {
      // we request a single song by id
      newData.songs = newData.songs.set(data.song.id, data.song);
    }
    return { ...state, data: newData };
  },
  [CLEAR_DATA]: (state, data) => {
    const newData = { ...state.data };

    if (Array.isArray(data.songs)) {
      data.songs.forEach(id => {
        newData.songs = newData.songs.delete(id);
      });
    } else if (typeof data.song === 'string') {
      // we request a single song by id
      newData.songs = newData.songs.delete(data.song);
    }
    return { ...state, data: newData };
  },
  [CLEAR_ALL_DATA]: state => ({ ...state, data: { songs: Map() } }),
};

export default (state = initialState, { value, type } = {}) => {
  const fn = reducers[type];
  if (fn) {
    return fn(state, value);
  }
  if (type === ALL) {
    return value.reduce((acc, { value: val, type: t }) => {
      if (reducers[t]) {
        return reducers[t](acc, val);
      }
      return acc;
    }, state);
  }
  return state;
};

// store's data property contains only immutable Maps
