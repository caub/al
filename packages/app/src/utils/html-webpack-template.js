// used by webpack.config.js

module.exports = ({ htmlWebpackPlugin }, appHtml = '', appCss = '') => `<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="preload" href="//fonts.googleapis.com/css?family=Roboto:300,400,500" as="style">
	<link rel="preload" href="https://fonts.googleapis.com/icon?family=Material+Icons" as="style"/>
	<title>${htmlWebpackPlugin.options.title}</title>
	<script src="https://apis.google.com/js/api.js" async></script>
</head>

<body>
	<div id="root">${appHtml}</div>
	<style id="jss-server-side">${appCss}</style>
</body>
</html>`;
