import { delay } from 'utils';

export const ALL = 'ALL'; // execute multiple actions
export const FLASH = 'FLASH'; // flash messages
export const CURRENT_USER = 'CURRENT_USER'; // user profile data
export const DATA = 'DATA'; // gql api data
export const CLEAR_DATA = 'CLEAR_DATA'; // clear data entries in cache
export const CLEAR_ALL_DATA = 'CLEAR_ALL_DATA';

// dispatch multiple actions, and make sure to resolve any promise passed
export const all = actions =>
  Promise.all(
    actions.map(
      action =>
        action.value && typeof action.value.then === 'function'
          ? action.value.then(v => ({ type: action.type, value: v }))
          : action,
    ),
  ).then(values => ({ type: ALL, value: values }));

export const setFlash = ({ open = true, message, isError }) => ({
  type: FLASH,
  value: { open, message, isError },
});

/**
 * this will later user simply cookies, either client.query('{currentUser {email username}}') or a rest endpoint
 */
export const loadProfile = () => ({
  type: CURRENT_USER,
  value: localStorage.alProfile
    ? Promise.resolve().then(() => JSON.parse(localStorage.alProfile))
    : {
        name: 'Anonymous',
      },
});

/*  ^ equivalent to:
const getProfile = () => delay(450)
  .then(() => (localStorage.al_token ? JSON.parse(localStorage.al_token) : null))
  .finally(value => ({
    type: CURRENT_USER,
    value
  }));
*/

/**
 * for signing in, a promise can be passed as value
 * @param {*} body
 */
export const setProfile = async value => {
  const data = await value;
  localStorage.alProfile = data ? JSON.stringify(data) : '';
  return {
    type: CURRENT_USER,
    value,
  };
};

/**
 * set data to the store
 * @param {Object or Promise}
 */
export const setData = obj =>
  obj && typeof obj.then === 'function'
    ? obj.then(value => ({ type: DATA, value }))
    : { type: DATA, value: obj };

/* note:
const setData = obj => ({ type: DATA, value: obj });
// works too, but I prefer to keep a Promise as return value, to possibly chain it
*/

/**
 * clear cached data entries
 * @param {*} obj {dataKey: id or listOfIds}
 */
export const clearData = obj => ({ type: CLEAR_DATA, value: obj });

export const clearAllData = () => ({ type: CLEAR_ALL_DATA });
