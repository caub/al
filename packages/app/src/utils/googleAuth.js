import React from 'react';

const getUserData = user => ({
  name: user.getBasicProfile().getName(),
  email: user.getBasicProfile().getEmail(),
  token: user.getAuthResponse().access_token,
});

// access token can be verified on server using https://www.googleapis.com/plus/v1/people/me?access_token=..

export default () =>
  new Promise(resolve => {
    window.gapi.load('client:auth2', () => {
      window.gapi.auth2
        .init({
          client_id: process.env.GOOGLE_CLIENT_ID,
          scope: 'email',
        })
        .then(() => {
          // not a real promise..
          const auth2 = window.gapi.auth2.getAuthInstance();
          const user = auth2.currentUser.get();
          const profile = user.getBasicProfile();
          if (auth2.isSignedIn.get()) return resolve(getUserData(user));
          window.gapi.auth2.getAuthInstance().signIn();
          auth2.isSignedIn.listen(() => {
            resolve(getUserData(user));
          });
        });
    });
  });

export const googleIcon = (
  <svg viewBox="0 0 48 48" width="30" height="30">
    <defs>
      <path
        id="a"
        d="M44.5 20H24v8.5h11.8C34.7 33.9 30.1 37 24 37c-7.2 0-13-5.8-13-13s5.8-13 13-13c3.1 0 5.9 1.1 8.1 2.9l6.4-6.4A21.94 21.94 0 0 0 2 24c0 12.2 9.8 22 22 22 11 0 21-8 21-22 0-1.3-.2-2.7-.5-4z"
      />
    </defs>
    <clipPath id="b">
      <use href="#a" overflow="visible" />
    </clipPath>
    <path clipPath="url(#b)" fill="#FBBC05" d="M0 37V11l17 13z" />
    <path clipPath="url(#b)" fill="#EA4335" d="M0 11l17 13 7-6.1L48 14V0H0z" />
    <path clipPath="url(#b)" fill="#34A853" d="M0 37l30-23 7.9 1L48 0v48H0z" />
    <path clipPath="url(#b)" fill="#4285F4" d="M48 48L17 24l-4-3 35-10z" />
  </svg>
);
