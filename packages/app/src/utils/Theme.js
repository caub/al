import React from 'react';
import JssProvider from 'react-jss/lib/JssProvider';
import { create, SheetsRegistry } from 'jss';
import { loadCSS } from 'fg-loadcss/src/loadCSS';
import { MuiThemeProvider, createMuiTheme, jssPreset } from 'material-ui/styles';
import { purple, teal } from 'material-ui/colors';

if (process.browser) {
  const last = document.head.childNodes[document.head.childNodes.length - 1];
  loadCSS('https://fonts.googleapis.com/icon?family=Material+Icons', last); // maybe check if it exists first
  loadCSS('//fonts.googleapis.com/css?family=Roboto:300,400,500"', last);
}

export const theme = createMuiTheme({
  palette: {
    primary: purple,
    secondary: teal,
  },
  mixins: {
    globals: {
      body: {
        margin: 0,
      },
      '*': {
        boxSizing: 'border-box',
      },
      a: {
        textDecoration: 'none',
        color: 'inherit',
      },
      '[role=button]': {
        cursor: 'pointer',
      },
      strong: {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
      },
      'h1, h2, strong': {
        fontWeight: 500,
      },
    },
  },
});

// Configure JSS
const jss = create(jssPreset());

const sheetsManager = new Map();

const sheetsRegistry = new SheetsRegistry();

export default ({ children }) => (
  <JssProvider jss={jss} registry={sheetsRegistry}>
    <MuiThemeProvider theme={theme} sheetsManager={sheetsManager}>
      {children}
    </MuiThemeProvider>
  </JssProvider>
);
