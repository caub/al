import md5 from 'js-md5';
import IO from 'socket.io-client';

// export * from './actions'; // would be circular, not needed anyway

export const io = IO(process.env.SONG_HOST);
io.on('connect', () => console.log('connected'));
io.on('disconnect', () => console.log('disconnected'));

export const delay = (t, v) => new Promise(r => setTimeout(r, t, v));

export const gravatar = email => `https://s.gravatar.com/avatar/${md5(email)}?size=128&default=retro`;

export class FetchError extends Error {
  constructor(body) {
    const message = typeof body === 'string' ? body : body.message || 'API error';
    super(message);
  }
}

const JSON_RE = /^application\/json/;

const fetchHeaders = (endpoint, { body, headers, ...rest } = {}) => {
  const heads = {
    Accept: 'application/json',
    ...(body ? { 'Content-Type': 'application/json' } : undefined),
    ...headers,
  };
  return fetch(`${process.env.SONG_HOST}${endpoint}`, {
    headers: heads,
    // todo credentials: 'include',
    ...rest,
    ...(body && JSON_RE.test(heads['Content-Type']) ? { body: JSON.stringify(body) } : undefined),
  });
};

const toJson = r =>
  (JSON_RE.test(r.headers.get('Content-Type')) ? r.json() : r.text()).then(
    body => (r.ok ? body : Promise.reject(new FetchError(body))),
  );

/**
 * fetchApi helper
 * @param {*} endpoint relative endpoint to the api server
 * @param {*} fetchOpts: optional {method: 'POST' | 'PUT' (default 'GET') .., body: Object, headers: Object}
 */
export const fetchApi = (endpoint, opts) => fetchHeaders(endpoint, opts).then(toJson);

/**
 * fetch a GraphQL query
 * @param {*} query
 * @param {*} variables
 */
export const fetchGql = (query, variables) =>
  fetchApi('/gql', {
    method: 'POST', // todo try again with GET and URLSearchParams
    body: {
      query,
      variables,
    },
  }).then(({ data }) => data);
