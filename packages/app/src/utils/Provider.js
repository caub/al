import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from 'utils/reducers';

const promiseMiddleware = store => next => action => {
  if (typeof action.then === 'function') {
    return Promise.resolve(action).then(next);
  } else if (action.value && typeof action.value.then === 'function') {
    return Promise.resolve(action.value).then(value => next({ type: action.type, value }));
  }
  // else, not a promise
  try {
    return next(action);
  } catch (e) {
    return Promise.reject(e);
  }
};

// handle only remaining promises actions there, and only .catch, the rest should have been done before
const errorMiddleware = store => next => action => {
  if (!action.catch) return next(action);
  action.catch(e => {
    console.log('todo: report', e);
  });
};

export const store = createStore(reducers, applyMiddleware(thunk, promiseMiddleware, errorMiddleware));

export default ({ children, ...props }) => (
  <ReduxProvider store={store} {...props}>
    <BrowserRouter>{children}</BrowserRouter>
  </ReduxProvider>
);
