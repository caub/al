import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui';
import googleAuth, { googleIcon } from 'utils/googleAuth';
import { setProfile } from 'utils/actions';

window.googleAuth = googleAuth;

const styles = {
  container: {
    display: 'flex',
    justifyContent: 'center',
    margin: '3em',
  },
  label: {
    margin: 12,
  },
};

const Profile = ({ currentUser, classes, history, dispatch }) => (
  <div className={classes.container}>
    {currentUser.email ? (
      <div>
        <em>Connected with Google</em>
        <Button component={Link} to="/logout">
          Log out
        </Button>
      </div>
    ) : (
      <Button
        onClick={() => {
          googleAuth()
            .then(user => dispatch(setProfile(user)))
            .finally(() => {
              history.push('/');
            });
        }}
      >
        {googleIcon}
        <span className={classes.label}>Sign with Google</span>
      </Button>
    )}
  </div>
);

export default withStyles(styles)(connect(({ currentUser }) => ({ currentUser }))(Profile));
