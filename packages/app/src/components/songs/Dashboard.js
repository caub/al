import React from 'react';
import { Map, Set } from 'immutable';
import { connect } from 'react-redux';
import format from 'date-fns/format';
import { withStyles } from 'material-ui/styles';
import { CircularProgress } from 'material-ui/Progress';
import List, { ListItem, ListItemText } from 'material-ui/List';
import { MenuItem, MenuList } from 'material-ui/Menu';
import Checkbox from 'material-ui/Checkbox';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import Dropdown from 'components/shared/Dropdown';
import { fetchGql, io } from 'utils';
import { all, setFlash, setData, clearData } from 'utils/actions';

const styles = {
  selectAll: {
    display: 'inline-flex',
    alignItems: 'center',
    marginLeft: 16,
    padding: 0,
    background: 'rgba(0,0,0,.1)',
  },
  arrowDown: {
    marginLeft: -8,
    marginRight: 8,
  },
  item: {
    '& time + time': {
      marginLeft: '1rem',
    },
  },
};

const songListQuery = () =>
  fetchGql(`
  {
    songs {
      id
      name
      link
      duration
      created
    }
  }
`);

class Dashboard extends React.PureComponent {
  state = { selected: Set() };

  static defaultProps = {
    data: {
      songs: Map(),
    },
  };

  playSong = e => {
    const audio = e.target;
    if (audio.currentTime > 0) return; // hacky..

    io.emit('songListened', { id: audio.dataset.id });
  };

  componentDidMount() {
    this.container.addEventListener('play', this.playSong, true);

    this.props.dispatch(setData(songListQuery()));

    io.on('songListened', ({ songListened }) => {
      this.props.dispatch(setFlash({ message: `Someone's listening ${songListened.name}` }));
    });
  }
  componentWillUnmount() {
    this.container.removeEventListener('play', this.playSong, true);
  }

  deleteSelection = () => {
    const { selected } = this.state;
    return Promise.all(
      selected.map(id =>
        fetchGql(
          `
            mutation DelComment($id: ID!) {
              deleteSong(id: $id) {
                n
              }
            }
          `,
          { id },
        ),
      ),
    ).then(() => {
      this.setState({ selected: Set() });
      this.props.dispatch(
        all([clearData({ songs: [...selected] }), setFlash({ message: selected.size + ' Songs deleted' })]),
      );
    });
  };

  render() {
    const { selected } = this.state;
    const { classes, data: { loading, error, songs } = {} } = this.props;

    return (
      <div ref={el => (this.container = el)}>
        <Dropdown
          button={
            <Button className={classes.selectAll}>
              <Checkbox
                checked={songs.size ? selected.size === songs.size : false}
                onClick={e => e.stopPropagation()}
                onChange={() =>
                  this.setState({
                    selected: selected.size === songs.size ? Set() : Set([...songs.keySeq()]),
                  })
                }
                value="all"
              />
              <Icon className={classes.arrowDown}>arrow_drop_down</Icon>
            </Button>
          }
        >
          {({ onClose }) => (
            <MenuList component="nav">
              <MenuItem
                component={Button}
                onClick={() => this.setState({ selected: Set([...songs.keySeq()]) }, onClose)}
              >
                All
              </MenuItem>
              <MenuItem component={Button} onClick={() => this.setState({ selected: Set() }, onClose)}>
                None
              </MenuItem>
              {!!selected.size && (
                <MenuItem component={Button} onClick={() => this.deleteSelection().then(onClose)}>
                  Delete
                </MenuItem>
              )}
            </MenuList>
          )}
        </Dropdown>
        {loading ? (
          <CircularProgress size={50} />
        ) : error ? (
          <p>Error :(</p>
        ) : (
          <List>
            {songs.valueSeq().map(song => (
              <ListItem key={song.id} data-id={song.id}>
                <Checkbox
                  checked={selected.has(song.id)}
                  onChange={() =>
                    this.setState({
                      selected: selected.has(song.id) ? selected.delete(song.id) : selected.add(song.id),
                    })
                  }
                  value={song.id}
                />
                <ListItemText
                  className={classes.item}
                  primary={song.name}
                  secondary={
                    <>
                      <time>{song.duration}</time>
                      <time>{format(song.created, 'YYYY-MM-DD')}</time>
                    </>
                  }
                />
                <audio src={song.link} controls="controls" preload="none" data-id={song.id} />
              </ListItem>
            ))}
          </List>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(connect(({ data }) => ({ data }))(Dashboard));
