import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import { CircularProgress } from 'material-ui/Progress';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import { all, setFlash, setData, clearData } from 'utils/actions';

const styles = theme => ({
  root: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    left: theme.spacing.unit * 2,
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  notes: {
    marginTop: 0,
    marginBottom: 0,
    paddingBottom: 0,
  },
  loaderRoot: {
    height: 1,
  },
  loaderBar: {
    background: theme.palette.grey[500],
  },
  btn: {
    width: 32,
    height: 32,
  },
  hidden: {
    visibility: 'hidden',
    position: 'absolute',
    zIndex: -1,
  },
});

class UploadSong extends React.PureComponent {
  state = {};

  toggle = (e, open = !this.state.open) => {
    this.setState({ open });
  };

  upload = e => {
    e.preventDefault();
    const { files: [file] } = e.target;
    if (!file) {
      return;
    }
    // this.setState({ saving: true });
    // we can alreaydy display and play that song temporarily
    const tmpSong = {
      id: Math.random() + '',
      name: file.name,
      created: new Date().toISOString(),
      link: URL.createObjectURL(file),
      duration: 'n/a',
      // onLoad: () => {
      //   URL.revokeObjectURL(url);
      //   this.setState({ saving: false });
      // },
    };

    this.props.dispatch(setData({ song: tmpSong }));

    fetch(process.env.SONG_HOST + '/song?' + new URLSearchParams({ name: file.name }), {
      method: 'POST',
      body: file,
      headers: { 'Content-Type': file.type || 'application/octet-stream', 'Content-Length': file.size },
    })
      .then(r => r.json())
      .then(song => {
        this.setState({ saving: false });
        this.props.history.push('/');
        URL.revokeObjectURL(tmpSong.link);
        this.props.dispatch(
          all([
            clearData({ song: tmpSong.id }),
            setData({ song }),
            setFlash({ message: file.name + ' Uploaded successfully!' }),
          ]),
        );
      });
  };

  render() {
    const { classes } = this.props;
    const { saving } = this.state;
    const loaderClasses = { root: classes.loaderRoot, circle: classes.loaderBar };
    return (
      <div className={classes.root}>
        <input
          type="file"
          name="song"
          ref={el => (this.input = el)}
          accept="audio/*"
          className={classes.hidden}
          onChange={this.upload}
        />

        {saving ? (
          <CircularProgress value={100} variant="indeterminate" classes={loaderClasses} color="secondary" />
        ) : (
          <Button
            type="button"
            onClick={() => this.input.click()}
            variant="fab"
            color="primary"
            classes={{ label: classes.btn }}
          >
            <Icon>add</Icon>
          </Button>
        )}
      </div>
    );
  }
}

export default connect()(withStyles(styles)(withRouter(UploadSong)));
