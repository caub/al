import React from 'react';
import { connect } from 'react-redux';
import { delay } from 'utils';
import { all, setProfile, clearAllData } from 'utils/actions';

export default connect()(
  class LogoutView extends React.PureComponent {
    componentWillMount() {
      console.log('l');
      this.props.dispatch(all([clearAllData(), setProfile({ name: 'Anonymous' })]));
      delay(500).then(() => this.props.history.push('/'));
    }
    render() {
      return null;
    }
  },
);
