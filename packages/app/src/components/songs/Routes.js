import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import Dashboard from 'components/songs/Dashboard';
import SongDetails from 'components/songs/SongDetails';
import Profile from 'components/songs/Profile';
import Logout from 'components/songs/Logout';

export default () => (
  <Switch>
    <Route exact path="/" component={Dashboard} />
    <Route exact path="/home" render={() => <Redirect to="/" />} />
    <Route exact path="/s/:id" component={SongDetails} />
    <Route exact path="/me" component={Profile} />
    <Route exact path="/profile" component={Profile} />
    <Route exact path="/logout" component={Logout} />
    <Route render={() => <h2>Nothing here :(</h2>} />
  </Switch>
);
