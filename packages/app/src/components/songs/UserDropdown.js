import React from 'react';
import { Link } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Avatar from 'material-ui/Avatar';
import Tooltip from 'material-ui/Tooltip';
import Icon from 'material-ui/Icon';
import { MenuItem, MenuList } from 'material-ui/Menu';
import { ListItemIcon, ListItemText } from 'material-ui/List';
import { gravatar } from 'utils';
import Dropdown from 'components/shared/Dropdown';

const styles = {
  btn: {
    marginLeft: 16,
    color: 'white',
    '& strong': {
      marginLeft: 10,
    },
  },
  icon: {
    marginLeft: 10,
  },
};

const UserDropdown = ({ user = {}, classes }) => (
  <Dropdown
    button={
      <Button className={classes.btn}>
        <Tooltip title={user.name || ''}>
          <Avatar alt={user.email} src={user.email && gravatar(user.email)} />
        </Tooltip>
        <strong>{user.name}</strong>
        <Icon>arrow_drop_down</Icon>
      </Button>
    }
  >
    {({ onClose }) => (
      <MenuList component="nav" role="menu">
        <MenuItem disableGutters component={Link} to="/me" disableRipple onClick={onClose}>
          <ListItemIcon className={classes.icon}>
            <Icon>account_circle</Icon>
          </ListItemIcon>
          <ListItemText primary="Profile" />
        </MenuItem>
        <MenuItem disableGutters component={Link} to="/logout" disableRipple onClick={onClose}>
          <ListItemIcon className={classes.icon}>
            <Icon>power_settings_new</Icon>
          </ListItemIcon>
          <ListItemText primary="Log out" />
        </MenuItem>
      </MenuList>
    )}
  </Dropdown>
);

export default withStyles(styles)(UserDropdown);
