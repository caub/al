import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Route, Redirect, Switch, Link } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import { CircularProgress } from 'material-ui/Progress';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import Typography from 'material-ui/Typography';
import Flash from 'components/songs/Flash';
import Routes from 'components/songs/Routes';
import UserDropdown from 'components/songs/UserDropdown';
import UploadSong from 'components/songs/UploadSong';
import { setFlash } from 'utils/actions';

const styles = theme => ({
  '@global': {
    ...theme.mixins.globals,
    html: {
      ...theme.mixins.globals.html,
      background: theme.palette.background.default,
      fontFamily: theme.typography.fontFamily,
    },
  },
  root: {
    width: '100%',
    marginTop: 0,
    zIndex: 1,
  },
  appFrame: {
    display: 'flex',
    alignItems: 'stretch',
    minHeight: '100vh',
    width: '100%',
  },
  appBar: {
    background:
      'linear-gradient(180deg, rgba(0, 0, 0, .5), rgba(0, 0, 0, .5)), linear-gradient(90deg, #5504bd, #9c15c6)',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
  },
  fullPage: {
    position: 'fixed',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  home: {
    margin: '0 1rem',
  },
  title: {
    flex: 10,
    color: theme.palette.getContrastText(theme.palette.primary.main),
    textAlign: 'center',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  wrapSearch: {
    width: 400,
    textAlign: 'right',
    [theme.breakpoints.down('sm')]: {
      flex: 1,
    },
  },
  search: {
    position: 'relative',
    display: 'inline-flex',
    alignItems: 'center',

    '& > input': {
      order: 10,
      width: 250,
      color: 'white',
      paddingLeft: 56,
      fontSize: '1.2em',
      lineHeight: '1.9em',
      backgroundColor: 'rgba(255,255,255,.2)',
      outline: 'none',
      border: 'none',
      transition: 'all ease-in-out .3s',
      '&::placeholder': {
        color: 'rgba(255,255,255,.8)',
      },
      '&:focus': {
        width: 400,
        paddingLeft: 8,
      },
    },
    '& > .s-icon': {
      position: 'absolute',
      left: 10,
      fontSize: '2.3em',
      transition: 'all .3s',
    },
    '& > input:focus + .s-icon': {
      visibility: 'hidden',
      zIndex: -1,
    },
  },
  content: {
    width: '100%',
    backgroundColor: theme.palette.background.default,
    padding: `0 ${theme.spacing.unit}px`,
    margin: `75px auto 0`,
    maxWidth: 1280,
  },
});

export class App extends React.Component {
  state = {
    mobileOpen: false,
  };

  drawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };

  render() {
    const { classes, currentUser, location: { pathname } = {} } = this.props;

    return !currentUser ? (
      <div className={classes.fullPage}>
        <CircularProgress size={50} />
      </div>
    ) : (
      <div className={classes.root}>
        <div className={classes.appFrame}>
          <AppBar className={classes.appBar}>
            <IconButton color="inherit" className={classes.home} component={Link} rel="me" to="/home">
              <Icon>home</Icon>
            </IconButton>
            <Typography variant="display1" className={classes.title}>
              Music store
            </Typography>
            <div className={classes.wrapSearch}>
              <div className={classes.search}>
                <input placeholder="Search" />
                <Icon className="s-icon">search</Icon>
              </div>
            </div>
            <UserDropdown user={currentUser} />
          </AppBar>
          <div className={classes.content}>
            <Routes />
          </div>
        </div>
        <UploadSong />
        <Flash {...this.props.flash} onClose={() => this.props.setFlash({ open: false })} duration={4000} />
      </div>
    );
  }
}

export default withRouter(
  connect(({ flash, currentUser }, props) => ({ ...props, flash, currentUser }), {
    setFlash,
  })(withStyles(styles)(App)),
);
