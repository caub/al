import React from 'react';
import Snackbar from 'material-ui/Snackbar';
import { withStyles } from 'material-ui/styles';
import { red } from 'material-ui/colors';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';

/**
 * Used to show some notification/action acknowlegment to user
 */

const styles = {
  bg: {
    background: red[600],
  },
};

const Flash = ({
  message,
  open = Boolean(message),
  isError,
  duration = 1000,
  classes,
  onClose = () => {},
}) => (
  <Snackbar
    open={open}
    message={message}
    anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
    autoHideDuration={duration}
    onClose={onClose}
    SnackbarContentProps={{
      className: isError && classes.bg,
    }}
    action={
      <IconButton key="close" aria-label="Close" color="inherit" onClick={onClose}>
        <Icon>close</Icon>
      </IconButton>
    }
  />
);

export default withStyles(styles)(Flash);
