import React from 'react';
import { shallow } from 'enzyme';
import { CircularProgress } from 'material-ui/Progress';
import { App } from 'components/songs/App';
import Routes from 'components/songs/Routes';

it('should render a Loader when currentUser is undefined', () => {
  const wrapper = shallow(<App currentUser={undefined} classes={{}} />);
  expect(wrapper.find(CircularProgress)).toHaveLength(1);
});

it('should render Routes with a currentUser', () => {
  const wrapper = shallow(<App currentUser={{ email: 'test@gmail.com' }} classes={{}} />);
  expect(wrapper.find(Routes)).toHaveLength(1);
});
