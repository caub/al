const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
require('dotenv/config');
require('dotenv').load({ path: '~.env' });

const SONG_HOST = process.env.SONG_HOST;
const isProd = process.env.NODE_ENV && process.env.NODE_ENV !== 'development';

const VENDOR_LIBS = [
  'react',
  'react-dom',
  'react-router-dom',
  'redux',
  'react-redux',
  'classnames',
  'immutable',
  'material-ui',
  'jss',
  'react-jss',
  'react-popper',
];

module.exports = {
  entry: {
    app: './src/index.js',
    vendor: VENDOR_LIBS,
  },
  output: {
    path: `${__dirname}/dist`,
    filename: isProd ? '[name].js?v=[chunkhash:4]' : '[name].js',
    publicPath: '/',
  },
  mode: isProd ? 'production' : 'development',
  optimization: {
    splitChunks: { chunks: 'all' },
  },
  devServer: {
    hot: true,
    contentBase: 'dist',
    port: 9000,
    // let's proxy API thorugh webpack dev-server, we'd have to do that if we were using cookies
    proxy: {
      '/gql': 'http://localhost:3000',
      '/song': 'http://localhost:3000',
    },
  },
  devtool: !isProd ? 'inline-source-map' : undefined,
  resolve: {
    extensions: ['.js', '.json'],
    modules: ['node_modules', 'src'],
    alias: {
      assets: `${__dirname}/assets/`,
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      // {
      //  test: /\.s?css$/,
      //  use: ['style-loader', 'css-loader', { loader: 'resolve-url-loader', options: { fail: false } }],
      // },
      {
        test: /\.(svg|png|jpg|gif|cur|ttf|eot|woff2?)(\?[a-z0-9]+)?$/,
        loader: 'file-loader?name=[path][name].[ext]?v=[hash:4]',
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(isProd ? 'production' : ''),
        SONG_HOST: JSON.stringify(SONG_HOST),
        GOOGLE_CLIENT_ID: JSON.stringify(process.env.GOOGLE_CLIENT_ID),
      },
    }),
    new HtmlWebpackPlugin({
      // hash: true,
      favicon: 'assets/favicon.png',
      title: 'Music app',
      template: './src/utils/html-webpack-template.js',
      isProd,
      filename: 'index.html',
    }),
  ],
};
