const Enzyme = require('enzyme');
const EnzymeAdapter = require('enzyme-adapter-react-16');

require('dotenv').config({ path: '.env.test' });

global.fetch = require('fetch-cookie')(require('node-fetch'));
global.localStorage = {};
global.requestAnimationFrame = cb => setTimeout(cb, 0);
global.matchMedia = rule => ({ matches: rule.includes('min') });

// Setup enzyme's react adapter
Enzyme.configure({ adapter: new EnzymeAdapter() });
