import {
  GraphQLObjectType as ObjectType,
  GraphQLInputObjectType as InputObjectType,
  GraphQLNonNull as NonNull,
  GraphQLID as ID,
  GraphQLInt as Int,
  GraphQLFloat as Float,
  GraphQLString as String,
  GraphQLBoolean as Boolean,
  GraphQLList as List,
  GraphQLSchema as Schema,
} from 'graphql';
import { getSongs, mergeSong, deleteSong } from './songs';
import { getDownloadLink } from './apis/dropbox';
import { searchSpotify } from './apis/spotify';

// make name shorter for spotify search
const simplifyName = name =>
  name
    .split(/\W+/)
    .slice(0, 4)
    .filter(w => w.length > 4)
    .slice(0, 2)
    .join(' ');

const listArgs = {
  first: { type: Int },
  after: { type: String },
  orderBy: { type: String },
  desc: { type: Boolean },
  q: { type: String },
};

// const Page = (Type, prefix = '') =>
//   new ObjectType({
//     name: `Page${prefix}${Type}`,
//     description: `A simple pagination method
//  - nodes contains the actual list of data
//  - cursor is the end cursor or falsy if there are no more pages`,
//     fields: {
//       nodes: { type: new NonNull(new List(new NonNull(Type))) },
//       cursor: { type: String },
//     },
//   });

const DeletionResult = new ObjectType({
  name: 'DeletionResult',
  fields: {
    n: { type: Int },
  },
});

const Song = new ObjectType({
  name: 'Song',
  fields: {
    id: { type: new NonNull(ID) },
    name: {
      type: new NonNull(String),
      description: 'unique filename',
    },
    description: { type: String },
    title: { type: String },
    duration: { type: String },
    author: { type: String },
    format: { type: String },
    size: { type: Float },
    link: {
      type: String,
      resolve: ({ fileId }) => getDownloadLink(fileId),
    },
    artists: {
      type: new List(String),
      description: 'Artists fetched from Spotify',
      resolve: ({ name, title = name }) =>
        searchSpotify(simplifyName(title)).then(({ artists = [] }) => artists.map(o => o.name)),
    },
    popularity: {
      type: Int,
      description: 'Popularity fetched from Spotify',
      resolve: ({ name, title = name }) =>
        searchSpotify(simplifyName(title)).then(({ popularity }) => popularity),
    },
    created: { type: new NonNull(String) },
    updated: { type: String },
  },
});
// should use dataloader for caching artists + popularity when they are both requested

const SongInput = new InputObjectType({
  name: 'SongInput',
  fields: {
    id: { type: ID },
    name: { type: String },
    description: { type: String },
    link: { type: String },
  },
});

const QueryType = new ObjectType({
  name: 'Query',
  description: 'The root of all queries',
  fields: {
    rand: {
      type: Float,
      resolve: () => Math.random(),
    },

    songs: {
      type: new NonNull(new List(new NonNull(Song))),
      args: listArgs,
      resolve: (_, args) => getSongs(args),
    },
  },
});

const MutationType = new ObjectType({
  name: 'Mutation',
  description: 'The root of all mutations',
  fields: {
    mergeSong: {
      type: Song,
      args: { input: { type: new NonNull(SongInput) } },
      resolve: (_, { input }) => mergeSong(input),
    },
    deleteSong: {
      type: DeletionResult,
      args: { id: { type: new NonNull(ID) } },
      resolve: (_, { id }) => deleteSong(id),
    },
  },
});

export default new Schema({
  query: QueryType,
  mutation: MutationType,
});
