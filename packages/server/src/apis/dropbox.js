import { load } from 'dotenv';
import { Dropbox } from 'dropbox';
import fetch from 'node-fetch';

global.fetch = fetch; // required by dropbox

load({ path: '~.env' });

const dbx = new Dropbox({ accessToken: process.env.DROPBOX_TOKEN });

export const getDownloadLink = path => dbx.filesGetTemporaryLink({ path }).then(({ link }) => link);

export const download = path => dbx.filesDownload({ path });

/**
 * Upload data
 * @param {*}  { name, type (note used, dropbox use name extensions), stream }
 */
export const upload = ({ name, stream }) => dbx.filesUpload({ path: '/' + name, contents: stream });
