import { URLSearchParams } from 'url';
import fetch from 'node-fetch';

let spotifyToken;
/**
 * Get and refresh (every hour) Spotify API get token
 */
export const getSpotifyToken = async () => spotifyToken || autoRefreshSpotifyToken();

const autoRefreshSpotifyToken = () =>
  fetch('https://accounts.spotify.com/api/token', {
    method: 'POST',
    body: 'grant_type=client_credentials',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: `Basic ${Buffer.from(
        process.env.SPOTIFY_CLIENT_ID + ':' + process.env.SPOTIFY_CLIENT_SECRET,
      ).toString('base64')}`,
    },
  })
    .then(r => r.json())
    .then(({ access_token, expires_in }) => {
      spotifyToken = access_token;
      setTimeout(autoRefreshSpotifyToken, (expires_in - 10) * 1000);
      return spotifyToken;
    });

export const searchSpotify = q =>
  getSpotifyToken().then(token => {
    if (!q) return;
    return fetch(
      'https://api.spotify.com/v1/search?' +
        new URLSearchParams({
          q,
          type: 'track',
          limit: 1,
        }),
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then(r => r.json())
      .then(({ tracks: { items: [item = {}] = [] } = {} }) => item);
  });
