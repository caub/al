import fs from 'fs';
import os from 'os';
import { google } from 'googleapis';

// create a service account on https://console.developers.google.com/apis/credentials
// and download key as json or set it in GKEY env var
const keys = process.env.GKEY ? JSON.parse(process.env.GKEY) : require(os.homedir() + '/.gkey.json');

const jwtClient = new google.auth.JWT({
  email: keys.client_email,
  key: keys.private_key,
  scopes: [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.metadata',
    'https://www.googleapis.com/auth/drive.metadata.readonly',
  ],
});

const drive = google.drive({
  version: 'v3',
  auth: jwtClient,
});

let token;
try {
  token = require(os.homedir() + './.gtoken.json');
} catch (e) {
  //
}

// IMPORTANT: getToken must be called, and (todo) refreshed since it expires after ~1h
export const getToken = async () => {
  if (jwtClient.isTokenExpiring()) {
    // get a new one
    token = await jwtClient.authorize();
    fs.writeFile(os.homedir() + './.gtoken.json', JSON.stringify(token), () => {});
    return token;
  }
  return token;
};
// todo make setTimeout before expiry to renew token before it expires

/**
 * List song files (todo paginate)
 * https://developers.google.com/drive/v3/web/search-parameters
 */
export const list = opts =>
  drive.files
    .list({
      pageSize: 50,
      fields: 'files(id,name),nextPageToken',
      ...opts,
    })
    .then(({ data }) => data); // todo just make a dedicated folder

export const getDownloadLink = fileId =>
  drive.files
    .get({
      fileId,
      fields: 'webContentLink',
    })
    .then(({ data }) => data.webContentLink);

/**
 * Get file metadata
 * @param {*} fileId
 */
export const get = fileId =>
  drive.files
    .get({
      fileId,
      fields: 'id,name,mimeType,webContentLink',
    })
    .then(({ data }) => data);

/**
 * Download a file
 * @param {*} fileId
 * @param {Object} options
 * @returns Promise of an http response
 */
export const download = (fileId, opts) =>
  drive.files.get(
    {
      fileId,
      alt: 'media',
      ...opts,
    },
    { responseType: 'stream' },
  );

/**
 * Upload data
 * @param {*}  { name, type, stream }
 */
export const upload = ({ name, type, stream }) =>
  drive.files
    .create({
      resource: { name },
      media: { mimeType: type, body: stream },
      fields: 'id',
    })
    .then(async ({ data }) => {
      await share(data.id);
      return data;
    });

// todo use https://developers.google.com/drive/v3/web/resumable-upload rather, files could be big

const PUBLIC_SHARE = { type: 'anyone', role: 'reader', allowFileDiscovery: false };

export const share = fileId =>
  drive.permissions.create({
    resource: PUBLIC_SHARE,
    fileId,
    fields: 'id',
  });
