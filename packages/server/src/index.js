import 'dotenv/config';
import { start } from './server';

start()
  .then(server => {
    console.log('listening:', server.address().port);
  })
  .catch(console.error);
