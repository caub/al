import { promisify } from 'util';
import redis from 'redis';

// main redis client, shared by server and songs, closed by server
const client = redis.createClient({
  port: process.env.REDIS_PORT, // defaults to 6379
  host: process.env.REDIS_HOST, // defaults to localhost
});

export default client;

export const hgetall = promisify(client.hgetall).bind(client);
export const hmset = promisify(client.hmset).bind(client);
export const keys = promisify(client.keys).bind(client);
export const del = promisify(client.del).bind(client);
export const flushall = promisify(client.flushall).bind(client);
