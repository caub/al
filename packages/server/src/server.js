import { createServer } from 'http';
import express from 'express';
import graphqlHTTP from 'express-graphql';
import IO from 'socket.io';
import { cors } from './utils';
import schema from './schema';
import client from './redisClient';
import { uploadSong, getSong } from './songs';
import { getSpotifyToken } from './apis/spotify';

// import redisAdapter from 'socket.io-redis'; // if you want to enable multi-process mode
// io.adapter(redisAdapter({ host: process.env.REDIS_HOST, port: process.env.REDIS_PORT }));

// import session from 'express-session';
// import ConnectRedis from 'connect-redis';
// const RedisStore = ConnectRedis(session)

const app = express().disable('x-powered-by');

if (process.env.NODE_ENV !== 'production') {
  app.set('json spaces', 2);
}

app.use(cors);

// app.use(
//   session({
//     store: new RedisStore({ client }),
//     secret: process.env.SESSION_SECRET || 'very long secret',
//     resave: false,
//     saveUninitialized: false,
//   }),
// );

// app.get('/song/:id/download', downloadSong);
app.post('/song', uploadSong);

const server = createServer(app);

const io = IO(server);

app.use('/gql', graphqlHTTP({ schema, graphiql: true, context: { io } }));

/**
 * start app
 * @param port (optional, defaults to process.env.PORT || 3000)
 * @returns {Promise} server
 */
export const start = async (port = process.env.PORT || 3000) => {
  server.listen(port, () => {
    console.log(`Server is now running on http://localhost:${port}`);
    // Set up the WebSocket for handling GraphQL subscriptions
  });
  getSpotifyToken(); // prefetch this token, to go faster next call

  io.on('connection', socket => {
    console.log('socket connected', socket.id);
    socket.on('songListened', async ({ id }) => {
      const song = await getSong(id);
      socket.broadcast.emit('songListened', { songListened: song });
    });
  });

  return server;
};

export const close = () => {
  server.close();
  client.quit();
};
