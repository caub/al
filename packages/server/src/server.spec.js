import { start, close } from './server';
import { gql } from './utils';
import { flushall } from './redisClient';

const delay = (t, v) => new Promise(r => setTimeout(r, t, v));

beforeAll(async () => {
  await start(3002);
  await flushall();
  await delay(400);
});

afterAll(() => {
  close();
});

describe('gql tests', () => {
  it('finds no songs initially', async () => {
    const { data } = await gql(`{songs {id}}`);
    expect(data.songs).toHaveLength(0);
  });

  it('adds one song (metadata)', async () => {
    const { data } = await gql(`mutation addSong($input: SongInput!){mergeSong(input: $input) {id}}`, {
      input: { name: 'foo.mp3' },
    });
    expect(data.mergeSong.id).toBeTruthy();
  });

  it('deletes a song', async () => {
    const { data: { songs } } = await gql(`{songs {id}}`);
    expect(songs).toHaveLength(1);

    const { data } = await gql(`mutation delSong($id: ID!){deleteSong(id: $id) {n}}`, { id: songs[0].id });
    await delay(2000);
    const { data: { songs: songsAfter } } = await gql(`{songs {id}}`);
    expect(songsAfter).toHaveLength(0);
  });
});
