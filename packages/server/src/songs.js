import fs from 'fs';
import os from 'os';
import { PassThrough } from 'stream';
import mime from 'mime/lite';
import { uid, mediaInfo, streamFinish, fsStat } from './utils';
import { hgetall, hmset, keys, del } from './redisClient';
import { upload, getDownloadLink } from './apis/dropbox';

// API for songs metadata storage
export const SONGS_PREFIX = 'al_files_d:'; // redis and google-drive key prefix

// note: scan should be used instead of keys in prod, for proper pagination, if redis is still used for that

// orderBy and pagination not impl, will be easier with a proper DB (pg/mongo)
export const getSongs = async ({ first = 100, after, orderBy = 'name', desc, q } = {}) => {
  let ks = await keys(SONGS_PREFIX + '*');

  return Promise.all(ks.map(k => hgetall(k)));
};

export const getSong = id => hgetall(SONGS_PREFIX + id);

export const mergeSong = async o => {
  if (!Object.keys(o || {})) {
    throw new Error('Nothing to add/update');
  }
  if (!o.id) {
    o.id = uid();
  }
  const existingData = await hgetall(SONGS_PREFIX + o.id);
  const updatedData = existingData && { updated: new Date().toISOString() };
  const newData = { created: new Date().toISOString(), ...existingData, ...updatedData, ...o };
  await hmset(SONGS_PREFIX + o.id, newData);
  return newData;
};

// not deleting the binary (yet)
// we could just set a 'trashed' field too, to allow trashing
export const deleteSong = id => del(SONGS_PREFIX + id);

export const getSongData = stream => {
  const filePath = `${os.tmpdir()}/${SONGS_PREFIX}${uid()}`;
  const ws = fs.createWriteStream(filePath);
  stream.pipe(ws);
  return streamFinish(ws).then(() =>
    Promise.all([fsStat(filePath), mediaInfo(filePath).catch(() => ({}))]).then(([stats, data]) => ({
      ...data,
      size: stats.size,
      created: stats.ctime,
    })),
  );
};

/**
 * upload a song
 * @param {*} req
 * @param {*} res
 */
export const uploadSong = async (req, res) => {
  const type = req.headers['content-type'];
  const { name = `rand${Math.random()}.${mime.getExtension(type)}` } = req.query;

  // we want to use mediainfo, so need to store temporarity this file
  // todo: use PassThrough to pipe in parallel on drive and on fs
  try {
    const [file, songData] = await Promise.all([
      upload({ name: SONGS_PREFIX + name, type, stream: req.pipe(new PassThrough()) }),
      getSongData(req.pipe(new PassThrough())),
    ]);

    const [song, link] = await Promise.all([
      mergeSong({ ...songData, name, fileId: file.id, type }),
      getDownloadLink(file.id),
    ]);
    song.link = link;
    res.json(song);
  } catch (err) {
    res.status(400).end(err.message);
  }
};

/* // example for proper pagination with mongo:
let query = {},
  fromId,
  field = orderBy,
  value;

if (after) {
  // `after` is an opaque string containing last id in previous pagination, last orderBy and last value for that order By field
  [fromId, value, field = orderBy] = Buffer.from(after, 'base64')
    .toString()
    .split(':');
  query = {$or: [{[field]: {$ne: value}}, {id: {$gt: fromId}}]}; // secondary id sort, is to handle cases when the primary one has dups
}
if (q) {
  query.$text = {$search: q};
}
return coll.find(query).sort([[field, desc ? -1 : 1], ['id', 1]]).limit(first)
  .toArray()
  .then(songs => {
    if (songs.length < first) {
      return {nodes: songs, cursor: ''};
    }
    const last = songs[songs.length - 1];
    return {
      nodes: songs,
      cursor: `${last.id}:${last[field]}:${field}`,
    }
  })
*/
