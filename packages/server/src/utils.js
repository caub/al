import crypto from 'crypto';
import fs from 'fs';
import { promisify } from 'util';
import { exec } from 'child_process';
import { graphql } from 'graphql';
import schema from './schema';

export const fsStat = promisify(fs.stat);
export const readdir = promisify(fs.readdir);
export const mkdir = promisify(fs.mkdir);

const matchers = [
  ['duration', /^Duration\s*: (.*)$/m],
  ['author', /^Performer\s*: (.*)$/m],
  ['format', /^Format\s*: (.*)$/m],
  ['title', /^Track name\s*: (.*)$/m],
];

/**
 * get media infos, depends on mediainfo command
 * @param {*} path
 */
export const mediaInfo = path =>
  new Promise((res, rej) =>
    exec(`mediainfo "${path}"`, (err, data) => {
      if (err) return rej(err);
      const obj = {};
      matchers.forEach(([key, re]) => {
        const m = data.match(re);
        if (m && m[1]) obj[key] = m[1].trim();
      });
      res(obj);
    }),
  );

export const streamFinish = (stream, evtName) =>
  new Promise((res, rej) => {
    stream.on('finish', res);
    stream.on('error', rej);
  });

export const uid = (len = 12) =>
  crypto
    .randomBytes(len)
    .toString('base64')
    .replace(/=+$/, '')
    .replace(/\+/g, '-')
    .replace(/\//g, '_');

export const ORIGINS = /\/localhost:\d+$|[/.]stuff\.cool$/;

/**
 * CORS middleware
 */
export const cors = (req, res, next) => {
  const origin = req.headers.origin;
  const isAllowed = ORIGINS.test(origin);
  res.setHeader('Access-Control-Allow-Origin', isAllowed ? origin : '*');
  res.setHeader('Access-Control-Allow-Credentials', isAllowed);

  if (req.method.toUpperCase() === 'OPTIONS') {
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,SEARCH,POST,DELETE');
    res.setHeader('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
    res.setHeader('Access-Control-Max-Age', '86400');
    return res.status(204).end();
  }
  next();
};

export const gql = (source, variableValues) =>
  graphql({
    schema,
    source,
    variableValues,
  });

export const md5 = x =>
  crypto
    .createHash('md5')
    .update(x)
    .digest('hex');

export const gravatar = email => `https://gravatar.com/avatar/${md5(email)}?s=64&d=retro`;
