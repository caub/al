## Task

The assignment consist in building an Audio library/player:
The backend will serve the library (list of songs with metadata).
The frontend will display the library and play the songs.

Backend:
The backend should be written in Node.js and would be used as an API for serving songs and songs metadata.


Frontend:
The frontend should be written in React and use some UI library to help the task (Material-ui preferred but any other clean UI lib can be used).
The "app" should be a single page app which should display the complete music library and a player (think about a very simple iTunes).


Storage of songs/metadata:
Songs can be stored in a directory in the backend.
Metadata can be stored as JSON in a directory in the backend


Bonus:
Any bonus will be valued when we will review your code.


Suggested Bonuses:
Front end uses Redux.
Songs are stored/retrieved from some network storage (dropbox, drive, s3).
Metadata could be fetched from some external API instead of being statically stored in a JSON file.
Usage of websockets in order to implement some live information on what other users are listening (you are free to display that how you prefer).


## Infos

Songs are now stored on Dropbox or Google drive (see just below), metadata are extracted using [mediainfo](https://mediaarea.net/en/MediaInfo/Download), stored on redis (they could also be attached to stored files) and partly fetched from Spotify.
We list songs using redis, and stream (download/upload) using the external storage API (Dropbox/Drive)

requirement (one of):
- set up a Dropbox account https://www.dropbox.com/developers/apps, create an app and an access token
- set up a Google service account on https://console.developers.google.com/apis/credentials and download key as json or set it in GKEY env var, for usage in [src/googleDrive](packages/server/src/googleDrive.js)

commands:
- install: `npx lerna bootstrap`
- build: `npx lerna run build`
- test: `npx lerna run test`
- run (dev): `npm run dev` (:9000)
- run (prod): `npm start` (:8000)
- run (prod in docker): `npm run docker` (:8000) (make sure you stop other redis containers, ports might conflict, todo use external network/external_link to plug app with server)

todos:
- Add typings, interface for storage (drive/dropbox), ..
